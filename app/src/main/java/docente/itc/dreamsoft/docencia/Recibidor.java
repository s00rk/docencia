package docente.itc.dreamsoft.docencia;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

public class Recibidor extends BroadcastReceiver
{

    @Override
    public void onReceive(Context context, Intent intent)
    {

        Bundle extras = intent.getExtras();
        if (extras == null) {
            return;
        }

        String mensaje = extras.getString("message");
        int code = extras.getInt("codigo");
        if(mensaje == null)
        {
            return;
        }

        Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(2000);

        Intent i = new Intent(context, Recibidor.class);
        PendingIntent pending = PendingIntent.getBroadcast(context, 0, i,
                PendingIntent.FLAG_CANCEL_CURRENT);

        Notification myNotification = new NotificationCompat.Builder(context)
                .setContentTitle("ITC ~ Recordatorio!")
                .setContentText( mensaje )
                .setTicker("ITC!")
                .setWhen(System.currentTimeMillis())
                .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentIntent(pending)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(mensaje))
                .build();

        NotificationManager notificationManager =
                (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(code, myNotification);



    }
}
