package docente.itc.dreamsoft.docencia;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.Calendar;


public class ConfigActivity extends ActionBarActivity {


    CheckBox activar;
    EditText cantidadDias;
    JSONObject jObj = new JSONObject();
    Context ctx;
    SharedPreferences prefe;

    private void guardar()
    {
        SharedPreferences.Editor editor = prefe.edit();
        editor.putString("dias", cantidadDias.getText().toString());
        editor.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        ctx = this;

        activar = (CheckBox)findViewById(R.id.activarAlarma);
        cantidadDias = (EditText)findViewById(R.id.numeroDias);
        prefe = getSharedPreferences("datos",Context.MODE_PRIVATE);

        String dias = prefe.getString("dias", null);
        if(dias != null)
        {
            activar.setChecked(true);
            cantidadDias.setText(dias);
            cantidadDias.setEnabled(true);
        }

        activar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b == true)
                {
                    guardar();
                    Toast.makeText(ctx, "Alarma Activada", Toast.LENGTH_LONG).show();
                    cantidadDias.setEnabled(true);
                    try {
                        activarAlarma();
                    }catch(Exception e){
                    }
                }else{
                    Toast.makeText(ctx, "Alarma Desactivada", Toast.LENGTH_LONG).show();
                    cantidadDias.setEnabled(false);
                    cancelarAlarma();
                }
            }
        });

        cantidadDias.setFilters(new InputFilter[]{ new InputFilterMinMax("1", "20")});

        cantidadDias.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() > 0)
                {
                    guardar();
                }
            }
        });


    }


    private void cancelarAlarma()
    {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        for(Materia matt : ITCD.maestro.getMaterias()) {
            for (Tema temm : matt.getTemas()) {

                Intent updateServiceIntent = new Intent(this, Recibidor.class);
                PendingIntent pendingUpdateIntent = PendingIntent.getBroadcast(getApplicationContext(), Integer.parseInt(temm.getEvprogram()), updateServiceIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                try {
                    alarmManager.cancel(pendingUpdateIntent);
                } catch (Exception e) {
                    Log.e("ITC-Alarm", e.toString());
                }
            }
        }
    }
    private void activarAlarma()
    {
        cancelarAlarma();
        int x = 001;
        Calendar actual = Calendar.getInstance();
        Calendar cal;
        for(Materia matt : ITCD.maestro.getMaterias()) {
            for(Tema temm : matt.getTemas()) {
                cal = Calendar.getInstance();
                cal.set(Calendar.MONTH, temm.mes);
                cal.set(Calendar.YEAR, temm.anio);
                cal.set(Calendar.DAY_OF_MONTH, temm.dia);
                cal.set(Calendar.HOUR_OF_DAY, 12);
                cal.set(Calendar.MINUTE, 00);

                cal.add(Calendar.DAY_OF_MONTH, ((Integer.parseInt(cantidadDias.getText().toString()))*-1 ));
                if(cal.getTimeInMillis() <= actual.getTimeInMillis())
                {
                    continue;
                }


                Intent intent = new Intent(this, Recibidor.class);
                intent.putExtra("message", "La evaluacion para " + temm.getTema() + " es dentro de " + cantidadDias.getText().toString() + " dia(s)");
                intent.putExtra("codigo", x++);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(),Integer.parseInt(temm.getEvprogram()) , intent, PendingIntent.FLAG_CANCEL_CURRENT);
                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);
            }
        }
    }


    public void regresar(View view)
    {
        Intent returnIntent = new Intent();
        setResult(RESULT_CANCELED, returnIntent);
        finish();
    }
}
