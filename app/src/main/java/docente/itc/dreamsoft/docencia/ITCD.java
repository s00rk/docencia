package docente.itc.dreamsoft.docencia;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.*;

public class ITCD 
{
	private final static String url = "http://intertec.itculiacan.edu.mx/cgi-bin/sie.pl";
	public static Maestro maestro = null;
	private static byte intentos = 0, maxintentos = 5;

	
	public static void getMaterias()
	{
        intentos = 0;
		Login();
        if(maestro.getHash() == null)
            return;
		Hashtable<String, String> params = new Hashtable<String, String>();
		params.put("Opc", "HORARIODOC");
		params.put("Param", "PRDO");
		
		byte [] res = sendGet(params);

        try {
            Thread.sleep(1000);
        }catch(Exception a){}

		//Document doc = Jsoup.parse(res, "UTF-8");
        Document doc = null;
        try {
            doc = Jsoup.parse(new String(res, "ISO-8859-15"));
            Thread.sleep(1000);
        }catch(Exception e){}
        if(doc == null)
            return;
		
		Elements claves = Selector.select("table:eq(1) tr:gt(0) td:nth-child(1)", doc);
		Elements nombres = Selector.select("table:eq(1) tr:gt(0) td:nth-child(2)", doc);
		Elements horario = Selector.select("table:eq(1) tr:gt(0) td:nth-child(5)", doc);
				
		Materia matt;
		byte i = 0;
		for(Element e : claves)
		{			
			matt = new Materia();
			matt.setNombre( nombres.get(i).text().replace(" instrum.didact", "").trim() );
			matt.setClave( e.text().trim() );
			matt.setHorario( horario.get(i).text().trim() );
			i++;
			if(matt.getClave().length() > 3)
				maestro.addMateria(matt);
		}
	}
	
	public static void getTemas()
	{
		for(Materia materia : maestro.getMaterias())
		{
            /*
            intentos = 0;
			Login();
            if(maestro.getHash() == null)
                continue;
            */
			String dato1, dato2;
			dato1 = materia.getClave().substring(0, materia.getClave().length()-2);
			dato2 = materia.getClave().substring(materia.getClave().length()-2);

			Hashtable<String, String> params = new Hashtable<String, String>();
			params.put("Opc", "TEMARIODOC");
			params.put("MATERIA", dato1 + "%20%20%20" + dato2);
			
			byte [] res = sendGet(params);
            if(res == null)
                return;

            try {
                Thread.sleep(1000);
            }catch(Exception a){}

			
			//Document doc = Jsoup.parse(res, "UTF-8");
            Document doc = null;
            try {
                doc = Jsoup.parse(new String(res, "ISO-8859-15"));
                Thread.sleep(1000);
            }catch(Exception e){}
            if(doc == null)
                return;

			
			Elements temas = Selector.select("table:eq(1) tr:gt(1) td:nth-child(1)", doc);
			Elements pinicios = Selector.select("table:eq(1) tr:gt(1) td:nth-child(3)", doc);
			Elements pterminados = Selector.select("table:eq(1) tr:gt(1) td:nth-child(4)", doc);
			Elements rinicios = Selector.select("table:eq(1) tr:gt(1) td:nth-child(5)", doc);
			Elements rterminados = Selector.select("table:eq(1) tr:gt(1) td:nth-child(6)", doc);
			Elements eprogramada = Selector.select("table:eq(1) tr:gt(1) td:nth-child(7)", doc);
			Elements ereal = Selector.select("table:eq(1) tr:gt(1) td:nth-child(8)", doc);
			
			Tema temm;
			byte i = 0;
			for(Element e : temas)
			{
				temm = new Tema();
				temm.setTema(e.text().trim() );
				temm.setProgramadoInicio(pinicios.get(i).text().trim());
				temm.setProgramadoTermino(pterminados.get(i).text().trim());
				temm.setRealInicio(rinicios.get(i).text().trim());
				temm.setRealTermino(rterminados.get(i).text().trim());
				temm.setEvaluacionProgramada(eprogramada.get(i).text().trim());
				temm.setEvaluacionReal(ereal.get(i).text().trim());
				i++;
				if(temm.getTema().length() > 3)
					materia.addTema(temm);
			}
			
		}
	}


	private static byte [] sendGet(Hashtable<String, String> params)
	{

		try{
			String urlParameters = "?Control=" + maestro.getUsuario() + "&Password=" + maestro.getHash() + "&dummy=0&";
			Enumeration<String> enumKey = params.keys();
			while(enumKey.hasMoreElements()) {
			    String key = enumKey.nextElement();
			    String val = params.get(key);
			    urlParameters += key + "=" + val + "&";
			}
			urlParameters = urlParameters.substring(0, urlParameters.lastIndexOf("&"));

            Connection connectionTest = Jsoup.connect(url + urlParameters).method(Connection.Method.GET);
            connectionTest.timeout(7000);
            return connectionTest.execute().bodyAsBytes();
            /*
			URL obj = new URL(url + urlParameters);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	
			con.setRequestMethod("GET");
			
		 	BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
		 
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			
	
			return response.toString();
            */
		}catch(Exception e){
            if(e.getMessage() != null)
            {
                Log.e("ITC-ERROR", e.getMessage());

                Log.e("ITC-ERROR", e.toString());
            }
		}

		return null;
	}


	public static void Login() {
		try{
			maestro.setHash(null);
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		 
			con.setRequestMethod("POST");

				
			String urlParameters = "Opc=MAINDOCENTE&Control=" + maestro.getUsuario() + "&password=" + maestro.getContrasena();
		 
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());

			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();
		 
			int responseCode = con.getResponseCode();
		
			BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
		
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			
			if(responseCode == 200)
			{
				int pos = response.toString().indexOf("&Password=") + 10; 
				int pos2 = response.toString().indexOf("&dummy");
				if(pos > 10)
				{
					maestro.setHash( response.substring(pos, pos2) );
					intentos = 0;
				}else
					maestro.setHash(null);
			}else
				maestro.setHash(null);
			
		}catch(Exception e){
            if(e.getMessage() != null)
            {
                Log.e("ITC-ERROR", e.getMessage());

                Log.e("ITC-ERROR", e.toString());
            }
            maestro.setHash(null);
		}
		
		if(maestro.getHash() == null && intentos < maxintentos)
		{
			intentos++;
			Login();
		}
	 
	}

}
