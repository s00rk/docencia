package docente.itc.dreamsoft.docencia;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.Calendar;


public class MainActivity extends ActionBarActivity {

    EditText usuario, pass;
    ProgressBar progress;
    Context ctx;
    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        usuario = (EditText)findViewById(R.id.numeroControl);
        pass = (EditText)findViewById(R.id.contrasena);
        progress = (ProgressBar)findViewById(R.id.progressBar);
        usuario.setText("920");
        pass.setText("0731");
        ctx = MainActivity.this;
        handler = new Handler(Looper.getMainLooper());

    }

    public void Loguear(View view){
        if(usuario.getText().length() == 0)
        {
            showToast("El usuario no debe estar vacio!");
            return;
        }else if(pass.getText().length() == 0){
            showToast("La contraseña no debe estar vacia!");
            return;
        }
        progress.setVisibility(View.VISIBLE);


        java.lang.Thread empezar;
        empezar = new java.lang.Thread(new Runnable() {
            @Override
            public void run() {
                ITCD.maestro = null;
                ITCD.maestro = new Maestro(usuario.getText().toString(), pass.getText().toString());
                ITCD.getMaterias();
                if(ITCD.maestro.getHash() == null)
                {
                    showToast("Usuario/Contraseña Incorrecta!");
                }else if(ITCD.maestro.getMaterias().size() == 0) {
                    showToast("Error al obtener materias!");
                }else{
                    ITCD.getTemas();

                    stopProgress();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Intent i = new Intent(ctx, MateriaActivity.class);
                            startActivity(i);
                            finish();
                        }
                    });
                }
                stopProgress();
            }
        });
        empezar.start();

    }

    private void stopProgress()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progress.setVisibility(View.GONE);
            }
        });
    }

    public void showToast(final String toast)
    {
        runOnUiThread(new Runnable() {
            public void run()
            {
                Toast.makeText(ctx, toast, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void alaarma()
    {
        long time = Calendar.getInstance().getTimeInMillis() + (10*1000);

        Intent intent = new Intent(this, Recibidor.class);
        intent.putExtra("message", "La evaluacion para mat es dentro de 3 dia(s)");
        intent.putExtra("codigo", 002);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 20143102, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, time, pendingIntent);

        intent = new Intent(this, Recibidor.class);
        intent.putExtra("message", "La evaluacion para mat es dentro de 4 dia(s)");
        intent.putExtra("codigo", 001);
        pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 20143103, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, time+(10*1000), pendingIntent);


        pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.cancel(pendingIntent);

        Toast.makeText(this, "Alarma en 10 segundos",
                Toast.LENGTH_LONG).show();
    }

}
