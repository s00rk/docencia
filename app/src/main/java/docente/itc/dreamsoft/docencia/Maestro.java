package docente.itc.dreamsoft.docencia;

import java.util.ArrayList;
import java.util.List;


public class Maestro 
{
	private String usuario;
	private String contrasena;
	private String hash;
	private List<Materia> materias = new ArrayList<Materia>();
	
	public Maestro(String u, String p)
	{
		usuario = u;
		contrasena = p;
	}
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	public List<Materia> getMaterias() {
		return materias;
	}
	public void setMaterias(List<Materia> materias) {
		this.materias = materias;
	}
	public void addMateria(Materia materia)
	{
		this.materias.add(materia);
	}
	
	

}
