package docente.itc.dreamsoft.docencia;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.view.ViewGroup;


public class MateriaActivity extends ActionBarActivity {

    SparseArray<Group> groups = new SparseArray<Group>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materia);

        createData();
        ExpandableListView listView = (ExpandableListView) findViewById(R.id.expandableListView);
        ITCExpandableListAdapter adapter = new ITCExpandableListAdapter(this,
                groups);
        listView.setAdapter(adapter);

    }

    public void createData() {
        byte j = 0;
        for (Materia mat : ITCD.maestro.getMaterias())
        {
            Group group = new Group( mat.getNombre() );
            for(Tema tem : mat.getTemas())
            {
                group.children.add( tem.getTema() );
            }
            groups.append(j++, group);
        }
    }

    public void cerrarSesion(View view)
    {
        ITCD.maestro = null;
        Intent i = new Intent(MateriaActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }

    public void btnActivar(View view)
    {
        Intent i = new Intent(MateriaActivity.this, ConfigActivity.class);
        startActivityForResult(i, 1);
    }


}
