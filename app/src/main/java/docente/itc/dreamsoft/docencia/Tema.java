package docente.itc.dreamsoft.docencia;

public class Tema
{
	private String tema;
	private String programadoInicio;
	private String programadoTermino;
	private String realInicio;
	private String realTermino;
	private String evaluacionProgramada;
	private String evaluacionReal;
    private String evprogram;
    public int mes, anio, dia;
	
	public String getTema() {
		return tema;
	}
	public void setTema(String tema) {
		this.tema = tema;
	}
	public String getProgramadoInicio() {
		return programadoInicio;
	}
	public void setProgramadoInicio(String programadoInicio) {
		this.programadoInicio = programadoInicio;
        if(this.programadoInicio.length() > 0)
        {
            String anio = this.programadoInicio.substring(0, 2);
            String mes = this.programadoInicio.substring(2, 4);
            String dia = this.programadoInicio.substring(4, 6);
            this.programadoInicio = dia + "/" + mes + "/" + anio;
        }
	}
	public String getProgramadoTermino() {
		return programadoTermino;
	}
	public void setProgramadoTermino(String programadoTermino) {
		this.programadoTermino = programadoTermino;
        if(this.programadoTermino.length() > 0)
        {
            String anio = this.programadoTermino.substring(0, 2);
            String mes = this.programadoTermino.substring(2, 4);
            String dia = this.programadoTermino.substring(4, 6);
            this.programadoTermino = dia + "/" + mes + "/" + anio;
        }
	}
	public String getRealInicio() {
		return realInicio;
	}
	public void setRealInicio(String realInicio) {
		this.realInicio = realInicio;
        if(this.realInicio.length() > 0)
        {
            String anio = this.realInicio.substring(0, 2);
            String mes = this.realInicio.substring(2, 4);
            String dia = this.realInicio.substring(4, 6);
            this.realInicio = dia + "/" + mes + "/" + anio;
        }
	}
	public String getRealTermino() {
		return realTermino;
	}
	public void setRealTermino(String realTermino) {
		this.realTermino = realTermino;
        if(this.realTermino.length() > 0)
        {
            String anio = this.realTermino.substring(0, 2);
            String mes = this.realTermino.substring(2, 4);
            String dia = this.realTermino.substring(4, 6);
            this.realTermino = dia + "/" + mes + "/" + anio;
        }
	}
	public String getEvaluacionProgramada() {
		return evaluacionProgramada;
	}
	public void setEvaluacionProgramada(String evaluacionProgramada) {
		this.evaluacionProgramada = evaluacionProgramada;
        if(this.evaluacionProgramada.length() > 0)
        {
            this.evprogram = evaluacionProgramada;
            String anio = this.evaluacionProgramada.substring(0, 2);
            String mes = this.evaluacionProgramada.substring(2, 4);
            String dia = this.evaluacionProgramada.substring(4, 6);
            this.evaluacionProgramada = dia + "/" + mes + "/" + anio;
            this.dia = Integer.parseInt(dia);
            this.mes = Integer.parseInt(mes);
            this.anio = Integer.parseInt(anio);
        }
	}
	public String getEvaluacionReal() {
		return evaluacionReal;
	}
	public void setEvaluacionReal(String evaluacionReal) {
		this.evaluacionReal = evaluacionReal;
        if(this.evaluacionReal.length() > 0)
        {
            String anio = this.evaluacionReal.substring(0, 2);
            String mes = this.evaluacionReal.substring(2, 4);
            String dia = this.evaluacionReal.substring(4, 6);
            this.evaluacionReal = dia + "/" + mes + "/" + anio;
        }
	}
	public String getEvprogram()
    {
        return this.evprogram;
    }
	

}
