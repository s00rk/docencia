package docente.itc.dreamsoft.docencia;

import java.util.ArrayList;
import java.util.List;


public class Materia 
{
	private String clave;
	private String nombre;
	private String horario;
	private List<Tema> temas = new ArrayList<Tema>();
	
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getHorario() {
		return horario;
	}
	public void setHorario(String horario) {
		this.horario = horario;
	}
	public List<Tema> getTemas() {
		return temas;
	}
	public void setTemas(List<Tema> temas) {
		this.temas = temas;
	}
	public void addTema(Tema tema)
	{
		this.temas.add(tema);
	}
	

}
